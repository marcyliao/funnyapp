# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
FunnyApp::Application.config.secret_key_base = 'eff1ddb7a37ffc30ee8cf59816165194ae5ad1c507e3e9c62afc01791c8674deb0b07236cae1c6487fc55b6aee830f96efb98e7b994640736b93f139521d8f71'
