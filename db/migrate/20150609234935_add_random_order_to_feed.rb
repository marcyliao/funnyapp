class AddRandomOrderToFeed < ActiveRecord::Migration
  def change
    add_column :feeds, :random_order, :integer
    add_index(:feeds, :random_order)
  end
end
