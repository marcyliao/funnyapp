class AddedFeedTagAssociation < ActiveRecord::Migration
   def change
    create_table :feeds_tags do |t|
      t.integer :feed_id
      t.integer :tag_id

      t.timestamps
    end
  end
end
