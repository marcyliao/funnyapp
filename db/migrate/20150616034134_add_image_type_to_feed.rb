class AddImageTypeToFeed < ActiveRecord::Migration
  def change
    add_column :feeds, :image_type, :string
    add_index(:feeds, :image_type)
  end
end
