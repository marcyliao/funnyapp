class CreateFeeds < ActiveRecord::Migration
  def change
    create_table :feeds do |t|
      t.string :title
      t.text :text
      t.integer :priority
      t.boolean :is_active
      t.integer :up
      t.integer :down
      t.string :source
      t.string :source_url

      t.timestamps
    end
  end
end
