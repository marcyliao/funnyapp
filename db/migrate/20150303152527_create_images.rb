class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.boolean :is_active
      t.string :source_url
      t.integer :original_width
      t.integer :original_height
      t.belongs_to :feed
      t.attachment :attachment
      t.attachment :thumbnail
      t.timestamps
    end
  end
end
