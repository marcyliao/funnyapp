# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150616034134) do

  create_table "feeds", force: true do |t|
    t.string   "title"
    t.text     "text"
    t.integer  "priority"
    t.boolean  "is_active"
    t.integer  "up"
    t.integer  "down"
    t.string   "source"
    t.string   "source_url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "random_order"
    t.string   "image_type"
  end

  add_index "feeds", ["image_type"], name: "index_feeds_on_image_type", using: :btree
  add_index "feeds", ["random_order"], name: "index_feeds_on_random_order", using: :btree

  create_table "feeds_tags", force: true do |t|
    t.integer  "feed_id"
    t.integer  "tag_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "images", force: true do |t|
    t.boolean  "is_active"
    t.string   "source_url"
    t.integer  "original_width"
    t.integer  "original_height"
    t.integer  "feed_id"
    t.string   "attachment_file_name"
    t.string   "attachment_content_type"
    t.integer  "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.string   "thumbnail_file_name"
    t.string   "thumbnail_content_type"
    t.integer  "thumbnail_file_size"
    t.datetime "thumbnail_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tags", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
