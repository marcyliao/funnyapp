class Image < ActiveRecord::Base
  
  has_attached_file :thumbnail, :styles => { :original => ["300x300>",:jpg], :small => ["100x100>", :jpg] }, :default_url => "/images/thumbnail/:style/missing.png"
  validates_attachment_content_type :thumbnail, :content_type => /\Aimage\/.*\Z/
  validates :thumbnail, attachment_presence: true
  validates_with AttachmentPresenceValidator, attributes: :thumbnail
  validates_with AttachmentSizeValidator, attributes: :thumbnail, less_than: 4.megabytes
  
  has_attached_file :attachment, :default_url => "/images/attachment/:style/missing.png"
  validates_attachment_content_type :attachment, :content_type => /\Aimage\/.*\Z/
  validates :attachment, attachment_presence: true
  validates :attachment_file_size, numericality: { less_than: 4000000 }
  validates_with AttachmentPresenceValidator, attributes: :attachment
  validates_with AttachmentSizeValidator, attributes: :attachment, less_than: 10.megabytes
  
  belongs_to :feed
  
  before_save :extract_dimensions
  
  private
  
  # extract the size of the image from the original file and
  # store them to the width and height properties of the model
  def extract_dimensions
    return unless attachment_is_image?
    tempfile = attachment.queued_for_write[:original]
    unless tempfile.nil?
      geometry = Paperclip::Geometry.from_file(tempfile)
      self.original_width = geometry.width.to_i
      self.original_height = geometry.height.to_i
    end
  end
  
  # Helper method to determine whether or not an attachment is an image.
  # @note Use only if you have a generic asset-type model that can handle different file types.
  def attachment_is_image?
    attachment_content_type =~ %r{^(image|(x-)?application)/(bmp|gif|jpeg|jpg|pjpeg|png|x-png)$}
  end

end
