class Tag < ActiveRecord::Base
  has_and_belongs_to_many :feeds
  
  def self.feeds_count
    stats = {}
    stats['all'] = Feed.count
    Tag.all.each do |t|
      stats[t.name] = t.feeds.count
    end
    
    stats['gif'] = Image.where( attachment_content_type: 'image/gif' ).count
    stats['non-gif'] = Image.where.not( attachment_content_type: 'image/gif' ).count
    
    stats
  end
  
  def self.tag_name(name)
    Tag.where(name:name).first
  end
end
