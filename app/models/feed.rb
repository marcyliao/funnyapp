class Feed < ActiveRecord::Base
  has_many :images, dependent: :destroy
  has_and_belongs_to_many :tags
  validates :source_url, uniqueness: true, presence: true
  
  before_save :update_order_values
  before_save :update_image_type

  def update_order_values
    self.random_order = rand(0..1000000)
  end
  
  def update_image_type
    self.image_type = self.images.first.attachment_content_type if self.images.present? and self.images.first.attachment.present?
  end
  
  def self.update_order_values
    Feed.update_all( "random_order = 1000000*rand()" )
  end
end
