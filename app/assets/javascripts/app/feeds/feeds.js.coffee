jQuery ->
  feeds = []
  current_feed = null
  
  load_filter_params = ()->
    params = {}
    # load image type
    if $('#image-type-filter-group #gif-only').is(':checked')
      params.image_type="gif"
    else if $('#image-type-filter-group #non-gif').is(':checked')
      params.image_type="static"
      
    # load upload date
    if $('#upload-date-filter-group #last-week').is(':checked')
      params.start_date="week"
    else if $('upload-date-filter-group #last-month').is(':checked')
      params.start_date="month"
    else if $('upload-date-filter-group #last-year').is(':checked')
      params.start_date="year"
    
    tags_check_all = $('#tags-filter-group .all-tags')
    tags_checkboxes = $('#tags-filter-group .a-tag')
    if(tags_check_all.filter(':checked').length == 0 && tags_checkboxes.filter(':checked').length > 0)
      tags = []
      tags_checkboxes.filter(':checked').each ()->
        tags.push $(this).attr('data-id')
      params.tags = tags
    
    params
  
  find_feed_by_id = (id) ->
    for f in feeds
      if f.id == id
        return f
     return null
  
  load_feed = (feed) ->
    # clear the feed content
    $('.feed-holder').html('')
    # clear previous height
    $('.feed-container').css('height', '100%')
    
    feed_holder = $('.feed-holder')
    feed_holder.attr('data-id', feed.id)
    
    # add the images
    for img in feed.images
      feed_holder.append($('<img>').attr('src', img.url).attr('class', 'feed-img').attr('alt', feed.title))
        
    # add the text content
    if feed.title != null
      feed_holder.append($('<h1></h1>').attr('class', 'feed-title').text(feed.title))
    if feed.text != null
      feed_holder.append($('<p></p>').attr('class', 'feed-content').text(feed.text))
    
    # add the tags
    tags_holder = $('<p class="tags-holder"></p>')
    feed_holder.append(tags_holder)
    if(feed.tags.length > 0)
      for t in feed.tags
        tags_holder.append($('<span class="label label-info label-tag"></span>').text(t.name).attr('data-id', t.id))
    if(feed.images[0].attachment_content_type == 'image/gif') 
      tags_holder.append($('<span class="label label-primary label-tag">GIF</span>'))
    
    # set the up and down values
    $('.down-count').text('('+feed.down+')')
    $('.up-count').text('('+feed.up+')')
    
    # vertical centerize the of feed
    if $('.feed-holder').outerHeight() <= $('.feed-container').height()
      $('.feed-container').css('height', $('.feed-container').height()+'px')
      $('.feed-holder').css('position', 'relative').css('top', '55%').css('transform', 'translateY(-55%)')
    else
      $('.feed-container').css('height', 'auto')
      $('.feed-holder').css('position', '').css('top', '').css('transform', '')
    
    # scroll to top
    window.scrollTo(0, 0);
    
    #TODO: refactor this shit
    list_container = $('.feed-list-container')
    list_container.html('')
    for f in feeds
      list_container.append('<div class="feed-img-thumb-holder"><a><img class="feed-img-thumb" src="'+f.images[0].thumb_url+'"></a></div>')
    
  rate_up = () ->
    feed_holder = $('.feed-holder')
    feed_id = parseInt(feed_holder.attr('data-id'))
    $.ajax
     method: 'post'
     url: '/app/feeds/'+feed_id+'/rate_up/';
     success: (data, textStatus, jqXHR) ->
       f = find_feed_by_id(feed_id)
       if f != null
         f.up = f.up+1
       
  rate_down = () ->
    feed_holder = $('.feed-holder')
    feed_id = parseInt(feed_holder.attr('data-id'))
    $.ajax
     method: 'post'
     url: '/app/feeds/'+feed_id+'/rate_down/';
     success: (data, textStatus, jqXHR) ->
       f = find_feed_by_id(feed_id)
       if f != null
         f.down = f.down+1
  
  prev_image = () ->
    new_index = feeds.indexOf(current_feed)-1
    if new_index >= 0
      current_feed = feeds[new_index]
      load_feed(current_feed)
    else
      bootbox.alert "No more image in the history..."
  
  next_image = () ->
    if feeds.length == 0 || feeds.indexOf(current_feed) > feeds.length - 7
      $.ajax
        url: "/app/feeds/random_feeds.json"
        dataType: "json"
        data: load_filter_params()
        success: (data, textStatus, jqXHR) ->
          for f in data.feeds
            f.imgs_tags = []
            for img in f.images
              f.imgs_tags.push($('<img>').attr('src', img.url).attr('class', 'feed-img'))
            feeds.push(f)
            
          if current_feed == null
            current_feed = feeds[0]
            load_feed(current_feed)
    
    if current_feed != null
      new_index = feeds.indexOf(current_feed)+1
      if new_index < feeds.length
        current_feed = feeds[new_index]
        load_feed(current_feed)
        if new_index > 20
          feeds.shift()
      else
        bootbox.alert "Can't load any more images, please check the interenet connection."
  
  # init events
  # init navigation
  $('.btn-prev').on 'click', ()->
    prev_image()
  $('.btn-next').on 'click', ()->
    next_image()
  $(".feed-container").touchwipe
    wipeLeft: () -> 
      next_image()
    wipeRight: () -> 
      prev_image()
    min_move_x: 35
    preventDefaultEvents: false
    
  # init rating buttons
  $('.btn-up').on 'click', ()->
    rate_up()
    next_image()
  $('.btn-down').on 'click', ()->
    rate_down()
    next_image()
    
  # init filters
  tags_check_all = $('#tags-filter-group .all-tags')
  tags_checkboxes = $('#tags-filter-group .a-tag')
  tags_check_all.on 'ifChecked', ()->
    tags_checkboxes.iCheck('check')
  tags_check_all.on 'ifUnchecked', ()->
    tags_checkboxes.iCheck('uncheck')
        
  tags_checkboxes.on 'ifChanged', ()->
    if(tags_checkboxes.filter(':checked').length == tags_checkboxes.length)
        tags_check_all.prop('checked', 'checked')
    else
      tags_check_all.removeProp('checked')
    tags_check_all.iCheck('update');
  
  next_image()
  
