jQuery ->
  $('input.icheck').iCheck
    checkboxClass: 'icheckbox_polaris',
    radioClass: 'iradio_polaris'
    
  $('.nav-filter').on 'click', ()->
    filter_menu = $('.filter-menu');
    if filter_menu.is(":visible")
      filter_menu.hide()
    else
      filter_menu.show()
      
  $('.nav-images-list').on 'click', ()->
    feed_list_menu = $('.feed-list-menu');
    if feed_list_menu.is(":visible")
      feed_list_menu.hide()
    else
      feed_list_menu.show()
  
  $(window).resize ()->
    filter_menu = $('.filter-menu');
    feed_list_menu = $('.feed-list-menu');
    if $(window).width() >= 960
      filter_menu.show()
      feed_list_menu.show()
    else
      filter_menu.hide()
      feed_list_menu.hide()
      
