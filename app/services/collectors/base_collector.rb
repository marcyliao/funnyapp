require 'nokogiri'
require 'net/http'
require 'open-uri'

class Collectors::BaseCollector

  # return a feed with default values
  def create_default_feed
    feed = Feed.new
    feed.priority = 3
    feed.is_active = true
    feed.up = 0
    feed.down = 0
    feed
  end
  
  # return a feed with default values
  def create_default_image(image_url)
    downloaded_image = open(image_url)
    image = Image.new
    image.is_active = true
    image.attachment = downloaded_image
    image.thumbnail = downloaded_image
    image
  end
  
  def get_response_body(path)
    Net::HTTP.get URI.parse(path)
  end

  # given an array of text content, select the best two options
  # potential to be the title and content of a feed object
  # the best options should not contain dirty words, promotion contents
  def get_the_best_two_content_options(content_options)

    # delete the duplicate text
    content_options = content_options.uniq

    # delect the options if they do not satisfy standard
    content_options.delete_if do |text| 
      !(option_match_standard? text)
    end

    best_option1 = (content_options.length >= 1 ? content_options[0].capitalize: nil)
    best_option2 = (content_options.length >= 2 ? content_options[1].capitalize: nil)

    [best_option1, best_option2]
  end
  
  def option_match_standard?(text)
    flag = true

    # delete if the length is too shot or  too long
    flag = false if (text.length < 3 or text.length > 100)

    # delete if the first word is number
    flag = false if flag and !!(text.split(" ")[0] =~ /\A[-+]?[0-9]+\z/)

    # delete if contains bad words
    filter_words = ['dick','dicks','pic','pics','image','images','pinterest','pin','picture','imgur','pictures', 'fuck', 'shit', 'slut', 'reasons', 'gif', 'gifs', 'sex', '.com', '.net']
    filter_words.each do |word|
      if text.downcase.include? word
      flag = false
      end
    end

    flag
  end

 
end