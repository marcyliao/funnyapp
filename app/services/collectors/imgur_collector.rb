require 'json'
class Collectors::ImgurCollector < Collectors::BaseCollector
  
  def parse_all(path,tags)
    data = JSON.parse(get_response_body(path))["data"]
    data.each do |d|
      begin
        next if d["is_album"]
        next if d["size"]  > 5000000
        
        source = "Imagur"
        source_url = "http://imgur.com/gallery/#{d["hash"]}"
        
        option1, option2 = get_the_best_two_content_options([d["title"]])
        
        feed = create_default_feed
        feed.source_url = source_url
        feed.title = option1
        feed.text = option2
        feed.source = "Imgur"
    
        image_url = "http://i.imgur.com/#{d["hash"]}#{d["ext"]}"
        image = create_default_image(image_url)
    
        feed.images << image
        
        feed.tags << tags
        
        feed.save
        puts "done"
      rescue => ex
        puts ex.message
      end
    end
    true
  end
  
  def batch_tag_import(tag_key,tags, page_start, page_end)
    ts = Tag.where('name in (?)', tags)
    
    for i in page_start..page_end
      parse_all "http://imgur.com/t/#{tag_key}/viral/page/#{i}/hit.json", ts
    end
  end
  
   def batch_search_import(keyword,tags, page_start, page_end)
     ts = Tag.where('name in (?)', tags)
     
    for i in page_start..page_end
      parse_all "http://imgur.com/search/score/all/page/#{i}/hit.json?q=#{keyword}", ts
    end
  end
  
end
