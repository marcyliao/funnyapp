class Collectors::PinterestCollector < Collectors::BaseCollector
  
  def parse_all_in_html(html, tags) 
    doc = Nokogiri::HTML(html)
    links = doc.css(".pinImageWrapper").collect{|l| "https://www.pinterest.com"+l.attributes["href"].to_s}
    links.each do |l|
      begin
        if Feed.where(source_url:l).count > 0
          next
        end
        parse(l, tags)
      rescue => ex
        puts ex.message
      end
    end
  end
  
  def parse(path, tags)
    html = get_response_body(path)
    doc = Nokogiri::HTML(html)

    # collect the content options from the doc
    content_options = collect_content_options(doc)

    # select the best two texts from the array
    option1, option2 = get_the_best_two_content_options(content_options)

    feed = create_default_feed
    feed.source_url = path
    feed.title = option1
    feed.text = option2
    feed.source = "Pinterest"

    image_url = doc.css(".pinImage")[0].attributes["src"].value
    image = create_default_image(image_url)

    feed.images << image
    
    feed.tags << tags

    feed.save
  end
  
  # check BaseCollector.collect_content_options
  def collect_content_options(doc)  
    # grab all the title, description and comments to the array
    content_options = []
    
    # the main title link blow the image
    if doc.css(".richPinNameLink")[0].present? and doc.css(".richPinNameLink")[0].text.present?
      content_options << doc.css(".richPinNameLink")[0].text.strip
    end
    
    # the first comment
    if doc.css(".nonCanonicalDesc")[0].present? and doc.css(".nonCanonicalDesc")[0].text.present?
      content_options << doc.css(".nonCanonicalDesc")[0].text.strip
    end
    
    # the other comments
    doc.css(".commentDescriptionContent").each do |comment|
      if comment.text.present?
        content_options << comment.text.strip
      end
    end
    
    content_options
  end
  
  
end
