class Cms::ImportController < ApplicationController
  layout "cms"
  def form_all_in_html_pinterest
    
  end
  
  def parse_all_in_html_pinterest
    tags = Tag.where(name: params[:tags])
    Collectors::PinterestCollector.new.parse_all_in_html(params[:html], tags)
    redirect_to cms_feeds_url 
  end
end
