class Cms::FeedsController < ApplicationController
  layout "cms"
  
  def index
    @feeds = Feed.includes(:tags, :images).order('id DESC').paginate(:page => params[:page], :per_page => 50)
  end

end
