class App::FeedsController < ApplicationController
  def index
  end
  
  def random_feeds
    num = 5
    @feeds = Feed
    
    after_date = nil
    if(params[:start_date] == 'week')
      after_date = 1.week.ago
    elsif(params[:start_date] == 'month')
      after_date = 1.month.ago
    elsif(params[:start_date] == 'year')
      after_date = 1.year.ago
    end
    @feeds = @feeds.where('created_at > ?', after_date) if after_date.present?
    
    if(params[:image_type] == 'gif')
      @feeds = @feeds.where(image_type: 'image/gif' )
    elsif(params[:image_type] == 'static')
      @feeds = @feeds.where('image_type != "image/gif"')
    end  
    
    @feeds = @feeds.joins(:tags).where(tags: { id: params[:tags] }) if params[:tags].present?
    
    @feeds = @feeds.order(:random_order).offset(rand(@feeds.count-num))
    @feeds = @feeds.limit(num)
  end
  
  def rate_up
    @feed = Feed.find(params[:id])
    @feed.up = @feed.up+1;
    @feed.save
    
    head :ok, content_type: "text/html"
  end
  
  def rate_down
    @feed = Feed.find(params[:id])
    @feed.up = @feed.up-1;
    @feed.save
    
    head :ok, content_type: "text/html"
  end
end


