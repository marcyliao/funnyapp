json.feeds @feeds do |json, feed|
  json.(feed, :id, :title, :text, :up, :down, :source, :source_url, :created_at, :updated_at)
  json.images feed.images do |img|
    json.(img, :id, :original_width, :original_height, :attachment_content_type, :created_at, :updated_at)
    json.thumb_url img.thumbnail.url
    json.url img.attachment.url
  end
  json.tags feed.tags do |tag|
    json.(tag, :id, :name)
  end
end