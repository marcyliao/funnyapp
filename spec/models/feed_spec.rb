require 'spec_helper'

describe Feed do
  it "has a valid factory" do
    expect(build(:feed)).to be_valid
  end
  
  it "has many images" do
    feed = create(:feed)
    image = create(:image_gif, feed: feed)
    expect(feed.images.first).to eq(image)
  end
  
  it "should be able to have multiple tags" do
    feed = create(:feed)
    feed.tags << create(:tag)
    expect(feed.tags.size).to eq(1)
  end
end
