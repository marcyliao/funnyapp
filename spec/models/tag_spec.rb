require 'spec_helper'

describe Tag do
  it "has a valid factory" do
    expect(build(:tag)).to be_valid
  end
  
  it "should be able to have multiple feeds" do
    tag = create(:tag)
    tag.feeds << create(:feed)
    expect(tag.feeds.size).to eq(1)
  end
end
