require 'spec_helper'
include ActionDispatch::TestProcess

describe Image do
  it "has a valid factory" do
    expect(build(:image_gif)).to be_valid
  end
  
  it "has a feed" do
    image = create(:image_gif)
    expect(image.feed).not_to be(nil)
  end
  
  describe "attachment" do
    describe "gif" do
      let!(:image) {create(:image_gif)}
      
      it "should set the content_type as gif" do
        expect(image.attachment_content_type).to eq("image/gif")
      end
      
      it "shoud extract the dimensions of the original file before save" do
        expect(image.original_width).to eq(500)
        expect(image.original_height).to eq(500)
      end
    end
    
    describe "jpg" do
      let!(:image) {create(:image_static)}
      
      it "should set the content_type as gif" do
        expect(image.attachment_content_type).to eq("image/jpg")
      end
      
      it "shoud extract the dimensions of the original file before save" do
        expect(image.original_width).to eq(360)
        expect(image.original_height).to eq(640)
      end
    end
  end
end
