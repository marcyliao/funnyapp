FactoryGirl.define do
  factory :image_static, class: Image do
    is_active true
    source_url "abc.com"
    original_width 100
    original_height 100
    
    attachment { fixture_file_upload(Rails.root.join( 'spec', 'resources', 'images', 'statics', 'image1.jpg'), 'image/jpg') }
    thumbnail { fixture_file_upload(Rails.root.join( 'spec', 'resources', 'images', 'statics', 'image1.jpg'), 'image/jpg') }
    
    association :feed, factory: :feed, source_url: "abc.com"
    
    factory :image_gif do
      attachment { fixture_file_upload(Rails.root.join( 'spec', 'resources', 'images', 'gifs', 'image1.gif'), 'image/gif') }
      thumbnail { fixture_file_upload(Rails.root.join( 'spec', 'resources', 'images', 'gifs', 'image1.gif'), 'image/gif') }
      
      association :feed, factory: :feed, source_url: "abcd.com"
    end
    
  end
end