FactoryGirl.define do
  factory :tag, class: Tag do
    name "funny"
    description "desc"
  end
end