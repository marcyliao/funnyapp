FactoryGirl.define do
  factory :feed, class: Feed do
    title "title"
    text "text"
    priority 1
    is_active true
    up 5
    down 0
    source "Pinterest User"
    source_url "ac.com"
  end
end